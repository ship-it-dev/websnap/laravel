<?php

namespace Tests\Unit;

use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Route;
use Mockery\MockInterface;
use Websnap\Laravel\Http\Middleware\Websnap;
use Websnap\Php\Client;

class MiddlewareTest extends \Tests\TestCase
{

    /**
     * @test
     */
    public function canSendResponseThroughWebsnap(): void
    {
        Route::get('foo', fn() => 'bar')->middleware(Websnap::class);

        $this->partialMock(Client::class, function (MockInterface $mock) {
            $mock->expects('screenshot')->times(1)->andReturn(
                new Response(200, ['Content-Type' => 'application/pdf'], 'foobar')
            );
        });

        $this
            ->get('foo')
            ->assertOk();

    }
}