<?php

namespace Tests\Unit\Facades;

use GuzzleHttp\Psr7\Response;
use Mockery\MockInterface;
use Tests\TestCase;
use Websnap\Laravel\Facades\Websnap;
use Websnap\Php\Client;

class WebsnapTest extends TestCase
{
    /**
     * @test
     */
    public function callsPdfMethod(): void
    {
        $this->partialMock(Client::class, function(MockInterface $mock) {
            $mock->expects('pdf')->times(1)->andReturn(
                new Response(200, body: 'foobar')
            );
        });

        Websnap::pdf('<h1>Hello world</h1>');

    }

    /**
     * @test
     */
    public function callsScreenshotMethod(): void
    {
        $this->partialMock(Client::class, function(MockInterface $mock) {
            $mock->expects('screenshot')->times(1)->andReturn(
                new Response(200, body: 'foobar')
            );
        });

        Websnap::screenshot('<h1>Hello world</h1>');
    }
}