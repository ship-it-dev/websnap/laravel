<?php

namespace Tests\Unit;

use GuzzleHttp\Psr7\Response;
use Mockery\MockInterface;
use Tests\TestCase;
use Websnap\Php\Client;

class ResponseTest extends TestCase
{
    /**
     * @test
     */
    public function addsPdfMethod(): void
    {

        $this->partialMock(Client::class, function (MockInterface $mock) {
            $mock->expects('pdf')->times(1)->andReturn(
                new Response(200, body: 'foobar')
            );
        });

        $response = response()->pdf('<h1>Hello world</h1>');

        $this->assertInstanceOf(\Illuminate\Http\Response::class, $response);
    }

    /**
     * @test
     */
    public function addsScreenshotMacro(): void
    {

        $this->partialMock(Client::class, function (MockInterface $mock) {
            $mock->expects('screenshot')->times(1)->andReturn(
                new Response(200, body: 'foobar')
            );
        });

        $response = response()->screenshot('<h1>Hello world</h1>');

        $this->assertInstanceOf(\Illuminate\Http\Response::class, $response);
    }
}