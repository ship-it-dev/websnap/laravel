<?php


namespace Websnap\Laravel\Support;


use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;

class ResponseConverter
{
    private ResponseFactory $responseFactory;

    public function __construct(
        ResponseFactory $responseFactory
    )
    {
        $this->responseFactory = $responseFactory;
    }


    public function convert(ResponseInterface $response): Response
    {
        return $this->responseFactory->make(
            $response->getBody()->getContents(),
            $response->getStatusCode(),
            [
                'Content-Type'   => $response->getHeader('Content-Type'),
                'Content-Length' => $response->getHeader('Content-Length'),
                'Cache-Control'  => $response->getHeader('Cache-Control')
            ]
        );
    }
}