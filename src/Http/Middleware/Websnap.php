<?php


namespace Websnap\Laravel\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Websnap
{

    public function handle(Request $request, Closure $next, string $query = '')
    {

        $response = $next($request);

        if (!$response instanceof Response || config('websnap.middlewareBypass')) {
            return $response;
        }

        parse_str($query, $options);

        $options = array_merge(
            $options,
            $request->input('websnap', [])
        );

        return response()->screenshot(
            $response->content(),
            $options
        );
    }
}
