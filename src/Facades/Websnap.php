<?php


namespace Websnap\Laravel\Facades;


use Illuminate\Support\Facades\Facade;
use Psr\Http\Message\ResponseInterface;
use Websnap\Php\Client;

/**
 * @method static ResponseInterface pdf(string $source, array $options = [])
 * @method static ResponseInterface screenshot(string $source, array $options = [])
 */
class Websnap extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Client::class;
    }
}
