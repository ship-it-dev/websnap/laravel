<?php


namespace Websnap\Laravel\Providers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;
use Websnap\Laravel\Facades\Websnap as WebsnapFacade;
use Websnap\Laravel\Support\ResponseConverter;

class MacroServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->registerMacros();
    }

    private function registerMacros(): void
    {
        Request::macro('isWebsnap', function () {
            /**
             * @var Request $this
             */
            return $this->hasHeader('X-Websnap-Version');
        });

        ResponseFactory::macro('pdf', function (string $url, array $options = []): Response {

            $response = WebsnapFacade::pdf($url, $options);

            /** @var ResponseConverter $converter */
            $converter = app(ResponseConverter::class);

            return $converter->convert($response);
        });

        ResponseFactory::macro('screenshot', function (string $url, array $options = []) {

            $response = WebsnapFacade::screenshot($url, $options);

            /** @var ResponseConverter $converter */
            $converter = app(ResponseConverter::class);

            return $converter->convert($response);
        });
    }

}