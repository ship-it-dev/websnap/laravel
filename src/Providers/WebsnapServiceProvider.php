<?php


namespace Websnap\Laravel\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Websnap\Laravel\Http\Middleware\Websnap;
use Websnap\Laravel\Support\ResponseConverter;
use Websnap\Php\Client;

class WebsnapServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/websnap.php',
            'websnap'
        );

        $this->getRouter()
            ->aliasMiddleware('websnap', Websnap::class);
    }

    private function getRouter(): Router
    {
        return $this->app->make(Router::class);
    }

    public function register()
    {
        parent::register();

        $this->app->register(MacroServiceProvider::class);

        $this->app->singleton(Client::class);
        $this->app->singleton(ResponseConverter::class);

        $this->app->when(Client::class)
            ->needs('$token')
            ->give(fn() => config('websnap.token'));
    }
}
